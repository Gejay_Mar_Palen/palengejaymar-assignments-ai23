<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'dXGH,1E_9-jQo!LJ_6!c.ru7IR_M8xDU`6kt|AexX.W3ZkaI4:]PS(|).jtPXdL&' );
define( 'SECURE_AUTH_KEY',  'EHEer4)*!(.XNh*Ub[]+`y|57<sf4{y+qi?G3nKaHRr`4B.G!3{vMsdJ](,2!B|L' );
define( 'LOGGED_IN_KEY',    'B4OZX0/5k6b@EH<^&&&&,qVzPz~[Bq1?H+<),0}I,W<lH`kz9z_S%)n{xSwg|o3A' );
define( 'NONCE_KEY',        'm[nxSjk}v91:(Wh+.P@aP(_<~}<Z<.gP.YaTBTc!zpkU1ghQ:u`K*4Vlp<&{*Dx&' );
define( 'AUTH_SALT',        '> &2iuN(5])52kI!,o{UUle)4qE}TNH[~OvN8AW2yI7vIE)iMZ#Hnd7#^Rh5}C-:' );
define( 'SECURE_AUTH_SALT', 'iX.fQh#!G1*Fy8l`v`fxIMXb%J&:RL+k%&p#KH+yc.s{^^:1{GHSSf`!A-vbriVS' );
define( 'LOGGED_IN_SALT',   'EHh~BO~-x,oB#m*bN ,]S>rk-~kl8RbTHA [kZ{Mr;x,Z>y>]PpDM:ZwDzTJ$V?/' );
define( 'NONCE_SALT',       '`uGWV_ bbmo-kau13itF$gX[u`RM:FvoVWAr$j2mEs&0&!XOh=DISaQ3+<o~V/xg' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
